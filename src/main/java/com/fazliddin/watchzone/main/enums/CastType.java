package com.fazliddin.watchzone.main.enums;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:04
 * @project watch-zone
 */
public enum CastType {

    CAST_ACTOR,       // kinoni aktyorlari
    CAST_DIRECTOR     // kinoni directorlari

}
