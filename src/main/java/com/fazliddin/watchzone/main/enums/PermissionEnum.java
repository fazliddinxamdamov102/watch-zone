package com.fazliddin.watchzone.main.enums;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  14:51
 * @project watch-zone
 */
public enum PermissionEnum {

    ADD_USER,
    EDIT_USER,
    VIEW_USER,
    VIEW_ALL_USER,
    DELETE_USER


}
