package com.fazliddin.watchzone.main.enums;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  14:39
 * @project watch-zone
 */
public enum Gender {

    MALE,
    FEMALE,
    OTHER

}
