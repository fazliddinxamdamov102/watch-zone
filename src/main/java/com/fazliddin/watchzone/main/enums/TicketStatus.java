package com.fazliddin.watchzone.main.enums;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:45
 * @project watch-zone
 */
public enum TicketStatus {

    NEW,         // YANGI
    PURCHASED,   // XARID QILINGAN
    REFUNDED     // QAYTARIB BERILGAN

}
