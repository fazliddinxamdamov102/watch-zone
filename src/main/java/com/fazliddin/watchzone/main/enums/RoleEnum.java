package com.fazliddin.watchzone.main.enums;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  14:39
 * @project watch-zone
 */
public enum RoleEnum {

    ROLE_ADMIN,
    ROLE_USER,
    ROLE_OTHER

}
