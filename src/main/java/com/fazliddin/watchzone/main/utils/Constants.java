package com.fazliddin.watchzone.main.utils;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  14:32
 * @project watch-zone
 */
public interface Constants {

    String DEFAULT_PAGE_SIZE = "10";

}
