//package com.fazliddin.watchzone.main.repository;
//
//import com.fazliddin.watchzone.main.model.Role;
//import com.fazliddin.watchzone.main.model.VerificationCode;
//import com.fazliddin.watchzone.main.payload.ApiResult;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.sql.Timestamp;
//import java.util.Optional;
//import java.util.UUID;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:15
// * @project watch-zone
// */
//public interface VerificationCodeRepo extends JpaRepository<VerificationCode, UUID> {
//    Optional<VerificationCode> checkVerificationCode(String phoneNumber, String verificationCode, Timestamp timestamp);
//}
