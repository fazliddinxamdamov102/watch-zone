package com.fazliddin.watchzone.main.repository;

import com.fazliddin.watchzone.main.model.User;
import com.fazliddin.watchzone.main.payload.ApiResult;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  15:15
 * @project watch-zone
 */
public interface UserRepo extends JpaRepository<User , UUID> {
    Optional<User> findByPhoneNumber(String username);
}
