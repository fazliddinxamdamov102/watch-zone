package com.fazliddin.watchzone.main.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:30
 * @project watch-zone
 */
public interface Cast extends JpaRepository<Cast, UUID> {

}
