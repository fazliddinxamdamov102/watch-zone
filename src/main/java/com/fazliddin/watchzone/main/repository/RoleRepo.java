//package com.fazliddin.watchzone.main.repository;
//
//import com.fazliddin.watchzone.main.enums.RoleEnum;
//import com.fazliddin.watchzone.main.model.Role;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.Optional;
//import java.util.UUID;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:15
// * @project watch-zone
// */
//public interface RoleRepo extends JpaRepository<Role, UUID> {
//
//    Optional<Role> findByRoleType(RoleEnum roleType);
//
//
//}
