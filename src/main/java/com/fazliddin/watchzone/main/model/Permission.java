//package com.fazliddin.watchzone.main.model;
//
//import com.fazliddin.watchzone.main.enums.PermissionEnum;
//import com.fazliddin.watchzone.main.templete.MainLong;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.Entity;
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:12
// * @project watch-zone
// */
//@EqualsAndHashCode(callSuper = true)
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Entity
//
//public class Permission extends MainLong {
//
//    @Enumerated(EnumType.STRING)
//    private PermissionEnum name;
//
//}
