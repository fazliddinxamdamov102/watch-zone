package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:44
 * @project watch-zone
 */

@Entity
@Table(name = "movie_session")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovieSession extends MainUUID {

    @ManyToOne
    private MovieAnnouncement movieAnnouncement;

    @ManyToOne
    private Hall hall;

    @ManyToOne
    private SessionDate startDate;

    @ManyToOne
    private SessionTime startTime;

    @ManyToOne
    private SessionTime endTime;

}
