package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:49
 * @project watch-zone
 */
@Entity
@Table(name = "hall")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Hall extends MainUUID {

    @Column(nullable = false, length = 50)
    private String name;

    @OneToMany(mappedBy = "hall", cascade = CascadeType.ALL)
    private List<Row> rows;

    private Double vipAddFeeInPercent;
}
