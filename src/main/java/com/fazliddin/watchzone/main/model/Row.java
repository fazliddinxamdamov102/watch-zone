package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:55
 * @project watch-zone
 */
@Entity
@Table(name = "rows")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Row extends MainUUID {
    @Column(nullable = false)
    private Integer number;

    @OneToMany(mappedBy = "row", cascade = CascadeType.ALL)
    private List<Seat> seats;

    @ManyToOne
    private Hall hall;
}
