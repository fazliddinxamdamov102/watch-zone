package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.enums.CastType;
import com.fazliddin.watchzone.main.templete.MainLong;
import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.*;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:03
 * @project watch-zone
 */
@Entity
@Table(name = "casts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cast extends MainUUID {
    @Column(nullable = false)
    private String fullName;

    @OneToOne
    private Attachment photo;

    @Enumerated(EnumType.STRING)
    private CastType castType;
}
