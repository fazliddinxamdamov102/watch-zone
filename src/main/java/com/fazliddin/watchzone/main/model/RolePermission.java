//package com.fazliddin.watchzone.main.model;
//
//import com.fazliddin.watchzone.main.templete.MainLong;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.*;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:12
// * @project watch-zone
// */
//
//@EqualsAndHashCode(callSuper = true)
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Entity
//
//public class RolePermission extends MainLong {
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Role role;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Permission permission;
//
//}
