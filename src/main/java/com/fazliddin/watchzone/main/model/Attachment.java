package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:44
 * @project watch-zone
 */
@Entity
@Table(name = "attachment")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Attachment extends MainUUID {

    private String fileName;
    private String contentType;
    private Long size;

}
