package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:56
 * @project watch-zone
 */

@Entity
@Table(name = "movies")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Movie extends MainUUID {

    @Column(nullable = false, length = 100)
    private String title;

    @Column(columnDefinition = "text")
    private String description;

    private Integer durationInMin;

    private Double minPrice;

    @OneToOne
    private Attachment coverImage;

    @Column(nullable = false)
    private String trailerVideoUrl;

    @Column(nullable = false)
    private LocalDate releaseDate;

    private Double budget;

    @ManyToOne
    private Distributor distributor;

    @Column(nullable = false)
    private Double distributorShareInPercentage;

    @ManyToMany
    private List<Cast> casts;

    @ManyToMany
    private List<Genre> genres;
}
