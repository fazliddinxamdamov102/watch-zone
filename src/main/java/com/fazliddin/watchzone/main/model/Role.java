//package com.fazliddin.watchzone.main.model;
//
//import com.fazliddin.watchzone.main.enums.PermissionEnum;
//import com.fazliddin.watchzone.main.enums.RoleEnum;
//import com.fazliddin.watchzone.main.templete.MainUUID;
//import lombok.*;
//import org.hibernate.annotations.SQLDelete;
//import org.hibernate.annotations.Where;
//
//import javax.persistence.*;
//import java.util.List;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  14:41
// * @project watch-zone
// */
//@Entity
//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
//@Builder
//@Where(clause = "deleted=false")
//@SQLDelete(sql = "update roles set deleted = false where id = ?")
//
//public class Role extends MainUUID {
//
//    @Column(unique = true , nullable = false)
//    private String  name;
//
//    @Column(name = "role_type", nullable = false)
//    @Enumerated(EnumType.STRING)
//    private RoleEnum roleType;
//}
