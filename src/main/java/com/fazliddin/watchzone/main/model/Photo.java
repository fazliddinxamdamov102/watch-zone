package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:24
 * @project watch-zone
 */
@Entity
@Table(name = "photos")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Photo extends MainUUID {

    @ManyToOne
    private Movie movie;

    @OneToOne
    private Attachment attachment;

}
