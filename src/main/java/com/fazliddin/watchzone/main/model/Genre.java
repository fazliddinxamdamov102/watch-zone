package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainLong;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:05
 * @project watch-zone
 */
@Entity
@Table(name = "genres")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Genre extends MainLong {
    private String name;
}
