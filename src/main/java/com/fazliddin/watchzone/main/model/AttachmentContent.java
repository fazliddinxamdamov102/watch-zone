package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:44
 * @project watch-zone
 */
@Entity
@Table(name = "attachment_content")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AttachmentContent extends MainUUID {

    private byte[] data;

    @OneToOne
    private Attachment attachment;
}
