package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainLong;
import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:01
 * @project watch-zone
 */
@Entity
@Table(name = "distributors")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Distributor extends MainLong {
    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "text")
    private String description;
}
