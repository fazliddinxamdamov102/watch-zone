package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:50
 * @project watch-zone
 */
@Entity
@Table(name = "session_date")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SessionDate extends MainUUID {
    private LocalDate date;
}
