package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:48
 * @project watch-zone
 */
@Entity
@Table(name = "movie_announcement")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovieAnnouncement  extends MainUUID {

    @OneToOne
    private Movie movie;

    private Boolean isActive;



}
