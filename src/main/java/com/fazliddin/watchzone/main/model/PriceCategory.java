package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:55
 * @project watch-zone
 */
@Entity
@Table(name = "price_category")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PriceCategory extends MainUUID {
    @Column(nullable = false, length = 50)
    private String name, color;

    @Column(nullable = false)
    private Double addAddFeeInPercent;


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "priceCategory")
    private List<Seat> seats;

}
