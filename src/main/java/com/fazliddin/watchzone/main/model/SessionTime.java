package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:51
 * @project watch-zone
 */
@Entity
@Table(name = "session_time")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SessionTime extends MainUUID {
    private LocalTime time;
}
