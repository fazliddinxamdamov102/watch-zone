package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.enums.Gender;
import com.fazliddin.watchzone.main.enums.LanguageEnum;
import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  14:36
 * @project watch-zone
 */
@Getter
@Setter
@Entity(name = "users")
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "set")
@Where(clause = "deleted=false")
@SQLDelete(sql = "update users set deleted = false where id = ?")

public class User extends MainUUID {

    @Column(nullable = false)
    private String fullName;

    @Column(nullable = false , unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private Gender gender;


//    @ManyToOne(fetch = FetchType.LAZY)
//    private Role role;
//
//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "user_permission",
//            joinColumns = {@JoinColumn(name = "user_id")},
//            inverseJoinColumns = {@JoinColumn(name = "permission_id")})
//    private Set<Permission> permissions;   //USERNING HUQUQLARI

//    @Column(name = "language")
//    @Enumerated(EnumType.STRING)
//    private LanguageEnum language;


//    @Builder.Default
//    private boolean accountNonExpired = true;
//    @Builder.Default
//    private boolean accountNonLocked = true;
//    @Builder.Default
//    private boolean credentialsNonExpired = true;
//    @Builder.Default
//    private boolean enabled = false;

//    public User(String fullname, String phoneNumber, Role roleUser , LanguageEnum language) {
//        this.phoneNumber = phoneNumber;
//        this.fullName = fullname;
//        this.language = language;
//        this.role = roleUser;
//    }

//    public boolean isAccountNonExpired() {
//        return this.accountNonExpired;
//    }
//
//    public boolean isAccountNonLocked() {
//        return this.accountNonLocked;
//    }
//
//    public boolean isCredentialsNonExpired() {
//        return this.credentialsNonExpired;
//    }
//
//    public boolean isEnabled() {
//        return this.enabled;
//    }


}
