package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Fazliddin Xamdamov
 * @date 29.03.2022  09:20
 * @project watch-zone
 */
@Entity
@Table(name = "purchase_histories")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PurchaseHistory extends MainUUID {
    @ManyToOne
    private User user;

    @ManyToOne
    private Ticket ticket;
}
