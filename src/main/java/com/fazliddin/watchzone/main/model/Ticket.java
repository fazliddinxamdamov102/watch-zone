package com.fazliddin.watchzone.main.model;

import com.fazliddin.watchzone.main.enums.TicketStatus;
import com.fazliddin.watchzone.main.templete.MainUUID;
import lombok.*;

import javax.persistence.*;

/**
 * @author Fazliddin Xamdamov
 * @date 28.03.2022  16:42
 * @project watch-zone
 */
@Entity
@Table(name = "ticket")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Ticket extends MainUUID {

    @ManyToOne
    private MovieSession movieSession;

    @ManyToOne
    private Seat seat;

    @OneToOne
    private Attachment qrCode;

    private Double price;

    @Enumerated(EnumType.STRING)
    private TicketStatus status = TicketStatus.NEW;

    @ManyToOne
    private User user;
}
