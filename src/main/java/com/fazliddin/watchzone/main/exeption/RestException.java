package com.fazliddin.watchzone.main.exeption;

import com.fazliddin.watchzone.main.common.MessageService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  16:02
 * @project watch-zone
 */
@Getter
@Setter
public class RestException extends RuntimeException{

    private String message;

    private HttpStatus status;


    public RestException(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public static RestException restThrow(String message, HttpStatus status) {
        return new RestException(message, status);
    }

    public static RestException notFound(String termin) {
        return new RestException(MessageService.notFound(termin), HttpStatus.NOT_FOUND);
    }


    public static RestException forbidden() {
        return new RestException(MessageService.getMessage("FORBIDDEN"), HttpStatus.FORBIDDEN);
    }

    public static RestException badRequest() {
        return new RestException(MessageService.getMessage("BAD_REQUEST"), HttpStatus.BAD_REQUEST);
    }

    public static RestException serverError() {
        return new RestException(MessageService.getMessage("INTERNAL_SERVER_ERROR"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
