//package com.fazliddin.watchzone.main.config;
//
//import com.fazliddin.watchzone.Auth.payload.UserPrincipal;
//import org.springframework.data.domain.AuditorAware;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//
//import java.util.Optional;
//import java.util.UUID;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:26
// * @project watch-zone
// */
//public class AuditorAwareImpl implements AuditorAware<UUID> {
//    @Override
//    public Optional<UUID> getCurrentAuditor() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication != null && authentication.isAuthenticated() && !authentication.getPrincipal().equals("anonymousUser")) {
//            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
//            return Optional.of(userPrincipal.getUser().getId());
//        }
//        return Optional.empty();
//    }
//}
