//package com.fazliddin.watchzone.main.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.domain.AuditorAware;
//import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
//
//import java.util.UUID;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:24
// * @project watch-zone
// */
//@Configuration
//@EnableJpaAuditing
//public class JpaAuditing {
//
//    @Bean
//    AuditorAware<UUID> auditorAware(){
//        return new AuditorAwareImpl();
//    }
//}
