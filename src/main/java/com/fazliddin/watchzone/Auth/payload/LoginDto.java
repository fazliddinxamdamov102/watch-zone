package com.fazliddin.watchzone.Auth.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  16:19
 * @project watch-zone
 */

@Getter
@Setter
@AllArgsConstructor
public class LoginDto {
    @NotBlank(message = "{USERNAME_REQUIRED}")
    private String username;

    @NotBlank(message = "{PASSWORD_REQUIRED}")
    private String password;

}
