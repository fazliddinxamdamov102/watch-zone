//package com.fazliddin.watchzone.Auth.payload;
//
//import com.fazliddin.watchzone.main.model.User;
//import lombok.Data;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import java.util.Collection;
//import java.util.Map;
//import java.util.Set;
//import java.util.stream.Collectors;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:28
// * @project watch-zone
// */
//@Data
//public class UserPrincipal implements UserDetails {
//
//    private final User user;
//    private Map<String, Object> attributes;
//
//    public UserPrincipal(User user) {
//        this.user = user;
//    }
//
//    public UserPrincipal(User user, Map<String, Object> attributes) {
//        this.user = user;
//        this.attributes = attributes;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        String roleName = user.getRole().getName();
//        Set<String> permissions = user.getPermissions().stream().map(p -> p.getName().name()).collect(Collectors.toSet());
//        permissions.add(roleName);
//        return permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
//    }
//
//    @Override
//    public String getPassword() {
//        return this.user.getPassword();
//    }
//
//    @Override
//    public String getUsername() {
//        return this.user.getPhoneNumber();
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return this.user.isAccountNonExpired();
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return this.user.isAccountNonLocked();
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return this.user.isCredentialsNonExpired();
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return this.user.isEnabled();
//    }
//
//
//    public Map<String, Object> getAttributes() {
//        return attributes;
//    }
//
//    public String getName() {
//        return user.getPhoneNumber();
//    }
//}
