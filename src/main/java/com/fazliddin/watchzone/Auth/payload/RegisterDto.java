package com.fazliddin.watchzone.Auth.payload;

import com.fazliddin.watchzone.main.enums.LanguageEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  16:31
 * @project watch-zone
 */
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class RegisterDto {

    @NotBlank(message = "{FIRST_NAME_REQUIRED}")
    private String fullname;

    @NotBlank(message = "{VERIFICATION_CODE_REQUIRED}")
    private String verificationCode;

    @Pattern(regexp = "\\+[9]{2}[8][0-9]{9}", message = "{WRONG_PHONE_NUMBER_FORMAT}")
    @NotBlank(message = "{PHONE_NUMBER_REQUIRED}")
    private String phoneNumber;

    private LanguageEnum language;
}
