package com.fazliddin.watchzone.Auth.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Fazliddin Xamdamov
 * @date 26.03.2022  10:48
 * @project watch-zone
 */

@AllArgsConstructor
@Setter
@Getter
public class CheckUserRegisterDto {

    private String accessToken;
    private String refreshToken;
    private boolean registered;

    public CheckUserRegisterDto() {
        this.accessToken = null;
        this.refreshToken = null;
        this.registered = false;
    }

    public CheckUserRegisterDto(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.registered = true;
    }

}
