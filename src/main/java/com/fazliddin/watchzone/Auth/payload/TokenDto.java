package com.fazliddin.watchzone.Auth.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  16:20
 * @project watch-zone
 */
@Getter
@Setter
@AllArgsConstructor

public class TokenDto {
    @NotBlank(message = "{ACCESS_TOKEN_REQUIRED}")
    private String accessToken;

    @NotBlank(message = "{REFRESH_TOKEN_REQUIRED}")
    private String refreshToken;

}
