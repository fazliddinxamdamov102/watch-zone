//package com.fazliddin.watchzone.Auth.service;
//
//import com.fazliddin.watchzone.main.common.MessageService;
//import com.fazliddin.watchzone.main.exeption.RestException;
//import com.fazliddin.watchzone.Auth.payload.*;
//import com.fazliddin.watchzone.Auth.security.JwtProvider;
//import com.fazliddin.watchzone.main.enums.RoleEnum;
//import com.fazliddin.watchzone.main.model.User;
//import com.fazliddin.watchzone.main.payload.ApiResult;
//import com.fazliddin.watchzone.main.repository.RoleRepo;
//import com.fazliddin.watchzone.main.repository.UserRepo;
//import com.fazliddin.watchzone.main.repository.VerificationCodeRepo;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpStatus;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.sql.Timestamp;
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  16:23
// * @project watch-zone
// */
//
//@Service
//@RequiredArgsConstructor
//public class AuthServiceImpl implements AuthService {
//
//    private final UserRepo userRepo;
//    private final RoleRepo roleRepository;
//    private final VerificationCodeRepo verificationCodeRepository;
////    private final JwtFilter jwtFilter;
//    private final JwtProvider jwtProvider;
//
//    @Value("${verification-code.expire-time}")
//    private Long verificationExpireTime;
//
//    @Value("${verification-code.limit}")
//    private Integer limit;
//
//
//    @Override
//    public ApiResult<?> signUp(RegisterDto dto) {
//        VerificationCode verificationCode = verificationCodeRepository.checkVerificationCode(dto.getPhoneNumber(), dto.getVerificationCode(), new Timestamp(System.currentTimeMillis()))
//                .orElseThrow(() -> RestException.restThrow(MessageService.getMessage("VERIFICATION_CODE_NOT_AVAILABLE"), HttpStatus.BAD_REQUEST));
//
//        Role roleUser = roleRepository.findByRoleType(RoleEnum.ROLE_USER)
//                .orElseThrow(() -> RestException.restThrow(MessageService.getMessage("ROLE_NOT_FOUND"), HttpStatus.NOT_FOUND));
//
//        User user = new User(dto.getFullname() , dto.getPhoneNumber(), roleUser, dto.getLanguage());
//        userRepo.save(user);
//
//        verificationCode.setUsed(true);
//        verificationCodeRepository.save(verificationCode);
//
//        String accessToken = jwtProvider.generateToken(dto.getPhoneNumber(), true);
//        String refreshToken = jwtProvider.generateToken(dto.getPhoneNumber(), false);
//
//        return ApiResult.successResponse(new CheckUserRegisterDto(accessToken, refreshToken));
//    }
//
//    @Override
//    public ApiResult<TokenDto> login(LoginDto loginDto) {
//        return null;
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user = userRepo.findByPhoneNumber(username).orElseThrow(RestException::forbidden);
//        return new UserPrincipal(user);
//    }
//}
