package com.fazliddin.watchzone.Auth.service;

import com.fazliddin.watchzone.Auth.payload.LoginDto;
import com.fazliddin.watchzone.Auth.payload.RegisterDto;
import com.fazliddin.watchzone.Auth.payload.TokenDto;
import com.fazliddin.watchzone.main.payload.ApiResult;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Fazliddin Xamdamov
 * @date 25.03.2022  16:06
 * @project watch-zone
 */
public interface AuthService extends UserDetailsService {

    ApiResult<?> signUp(RegisterDto dto);

    ApiResult<TokenDto> login(LoginDto loginDto);

}
