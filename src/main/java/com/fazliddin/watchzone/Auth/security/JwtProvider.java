//package com.fazliddin.watchzone.Auth.security;
//
//
//import com.fazliddin.watchzone.main.exeption.RestException;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Component;
//
//import java.util.Date;
//
//
///**
// * @author Fazliddin Xamdamov
// * @date 25.03.2022  15:56
// * @project watch-zone
// */
//@Component
//public class JwtProvider {
//
//    @Value("${jwt.secret-key}")
//    private String secretKey;
//
//    @Value("${jwt.expire.access_token}")
//    private long accessTokenExpire;
//
//    @Value("${jwt.expire.refresh_token}")
//    private long refreshTokenExpire;
//
//    public String generateToken(String phoneNumber, boolean forAccess) {
//        Date expire = new Date(System.currentTimeMillis() + (forAccess ? accessTokenExpire : refreshTokenExpire));
////        SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS256); //or HS384 or HS512
////        String base64Key = Encoders.BASE64.encode(key.getEncoded());
//        return Jwts
//                .builder()
//                .signWith(SignatureAlgorithm.HS512, secretKey)
//                .setSubject(phoneNumber)
//                .setIssuedAt(new Date())
//                .setExpiration(expire)
//                .compact();
//    }
//
//    public String getUsername(String token) {
//        try {
//            return Jwts
//                    .parser()
//                    .setSigningKey(secretKey)
//                    .parseClaimsJws(token)
//                    .getBody()
//                    .getSubject();
//        } catch (Exception e) {
//            throw RestException.forbidden();
//        }
//    }
//
//    public void validateToken(String token) {
//        Jwts
//                .parser()
//                .setSigningKey(secretKey)
//                .parseClaimsJws(token);
//    }
//
//}
