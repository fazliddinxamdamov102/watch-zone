package com.fazliddin.watchzone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WatchZoneApplication {

    public static void main(String[] args) {
        SpringApplication.run(WatchZoneApplication.class, args);
    }

}
